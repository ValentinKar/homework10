<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки

class Product implements ForSale   // продукт
{
	public static $living = false;  // статическое свойство
	public $category; // категория
	public $brand;  // марка
	public $color;  // цвет 
	private $price; // цена
	public $discount; // скидка
	public $type;   // 

	public function setPrice($price)  // изменяю цену
	{
		$this->price = $price;
	}

	public function getPrice()   // узнаю цену
	{
		if ($this ->discount) {
			return round($this->price - ($this->price * $this ->discount/100) );
		}
		else {
			return $this ->price;
		}
	}
}