<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки


class Duck extends Tangible implements Birds  // утка
{
	public static $living = true;  // статическое свойство
	public $name; 
	public $onTheWater = true;   // на воде
	public $onTheLand = false;   // на суше
	public $onTheAir = false;      // в воздухе


	public function __construct($krya)  // конструктор требует имя
	{
		$this->name = $krya; 
	}

		public function floats()  // плывет
		{
			$this->onTheWater = true;
			$this ->onTheLand = false;
			$this ->onTheAir = false;
		}

	public function walking()  // идет
	{
		$this ->onTheLand = true;
		$this->onTheWater = false;
		$this ->onTheAir = false;
	}

		public function fly()  // летит
		{
			$this ->onTheAir = true;
			$this ->onTheLand = false;
			$this->onTheWater = false;
		}
}


class Televisor extends Tangible implements WithRemoteControl 
{
	public static $living = false;  // статическое свойство

	private $brand;  // марка телевизора
	private $price;  // цена
	private $type;    // плазма или жидкокристаллический
	private $diagonal;  // диагональ
	public $work = false;  // работает или нет

	public function setBrand($text)  // изменяю марку телевизора
	{
		$this->brand = $text;
	}

	public function getBrand()   // узнаю марку телевизора
	{
		return $this->brand;
	}

		public function setType($text)  // изменяю тип телевизора
		{
			$this->type = $text; 
		} 

		public function getType()   // узнаю тип телевизора
		{
			return $this->type; 
		} 

	public function setDiagonal($text)  // изменяю диагональ телевизора
	{
		$this->diagonal = $text; 
	} 

	public function getDiagonal()   // узнаю диагональ телевизора
	{
		return $this->diagonal; 
	} 

		public function turnOn()   // включить телевизор
		{
			$this->work = true; 
			return $this -> work;
		}

		public function turnOff()   // выключить телевизор
		{
			$this->work = false; 
			return $this->work; 
		}
}


class AutoCar extends Tangible implements Transport   // машина
{
	public static $living = false;  // статическое свойство
	public static $staticCounter = 0;   // статическое свойство - счетчик

	public $brand; // марка машины
	public $color; // цвет 
	private $price; // цена
	private $type;   // легковая, грузовая или автобуc
	private $speed_max = 150;  // максимальная скорость
	public $course = 0;  // направление движения машины
	public $angle_rudder = 0;  // угол поворота руля
	public $speed = 0;      // скорость
	public $gas = 0;    // подача топлива

	public function __construct($brand)  // конструктор устанавливает марку
	{
		$this->brand = $brand; 
		self::$staticCounter++; 
	}

		public function setType($text)  // изменяю тип авто
		{
			$this->type = $text;
		}

		public function getType()   // узнаю тип авто
		{

			return $this->type;
		}

	public function courseNew($rotation_rudder, $time)  // изменяю направление движения
	{
		$this->angle_rudder = $this ->angle_rudder + $rotation_rudder; 
		$this->course = $this ->course + $this ->angle_rudder / 10 * $time; 
		return $this ->course;
	}

	public function speed($add_gas, $time)   // изменяю скорость движения
	{
		$this->gas = $this ->gas + $add_gas;
		$this->speed = $this ->speed + $this ->gas * 10; 
		return $this ->speed; 
	}
}