<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки

interface Birds   // птицы 
{
	public function fly(); 
}

interface WithRemoteControl  // с дистанционным управлением
{
	public function turnOn(); 
	public function turnOff(); 
}

interface Writers   // пищущие 
{
	public function write($text); 
}

interface Transport  // средства передвижения
{
	public function courseNew($rotation_rudder, $time);  // сменить курс
	public function speed($add_gas, $time);   // сменить скорость
}

interface ForSale   // на продажу
{
	public function setPrice($price);   // изменяю цену
	public function getPrice();       // узнаю цену
}