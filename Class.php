<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки


class Ballpen extends Product implements Writers  // шариковая ручка
{
	private $rod = true;  // есть стержень или нет
	private $ink = 100;  // сколько чернил осталось в стержне
	public $work = true;  // работает или нет - если есть кнопка как у старых авторучек

	public function turnOn()   // включить
	{
		$this->work = true; 
		return $this ->work;
	}

	public function turnOff()   // выключить
	{
		$this->work = false; 
		return $this->work; 
	}

		public function insertRod()    // вставить стержень
		{
			$this ->rod = true; 
			return $this ->rod;
		}

		public function getRod()    // достать стержень
		{
			$this ->rod = false; 
			return $this ->rod;
		}

	public function write($text)    // написать текст
	{ 
			if ($this ->rod === false) {
				exit;
			}

		$i = strlen($text); 
		$this ->turnOn(); 

			if ($this ->type = 'gel') {
			$this ->ink = $this ->ink - $i / 5;
			}

			if ($this ->type = 'roller') {
			$this ->ink = $this ->ink - $i / 10;
			}

		echo "$text"; 
		return ' (чернил осталось: ' . $this ->ink . ' ед.) <br />'; 
	}
}


class Duck extends Product implements Birds  // утка
{
	public $name; 
	public $onTheWater = true;   // на воде
	public $onTheLand = false;   // на суше
	public $onTheAir = false;      // в воздухе

	public function __construct($krya)  // конструктор требует имя
	{
		$this->name = $krya; 
	}

		public function floats()  // плывет
		{
			$this->onTheWater = true;
			$this ->onTheLand = false;
			$this ->onTheAir = false;
		}

	public function walking()  // идет
	{
		$this ->onTheLand = true;
		$this->onTheWater = false;
		$this ->onTheAir = false;
	}

		public function fly()  // летит
		{
			$this ->onTheAir = true;
			$this ->onTheLand = false;
			$this->onTheWater = false;
		}
}


class Televisor extends Product implements WithRemoteControl 
{
	private $diagonal;  // диагональ
	public $work = false;  // работает или нет

	public function setDiagonal($text)  // изменяю диагональ телевизора
	{
		$this->diagonal = $text; 
	} 

	public function getDiagonal()   // узнаю диагональ телевизора
	{
		return $this->diagonal; 
	} 

		public function turnOn()   // включить телевизор
		{
			$this->work = true; 
			return $this -> work;
		}

		public function turnOff()   // выключить телевизор
		{
			$this->work = false; 
			return $this->work; 
		}
}


class AutoCar extends Product implements Transport   // машина
{
	public static $staticCounter = 0;   // статическое свойство - счетчик

	private $speed_max = 150;  // максимальная скорость
	public $course = 0;  // направление движения машины
	public $angle_rudder = 0;  // угол поворота руля
	public $speed = 0;      // скорость
	public $gas = 0;    // подача топлива

	public function __construct($brand)  // конструктор устанавливает марку
	{
		$this->brand = $brand; 
		self::$staticCounter++; 
	}

	public function courseNew($rotation_rudder, $time)  // изменяю направление движения
	{
		$this->angle_rudder = $this ->angle_rudder + $rotation_rudder; 
		$this->course = $this ->course + $this ->angle_rudder / 10 * $time; 
		return $this ->course;
	}

	public function speed($add_gas, $time)   // изменяю скорость движения
	{
		$this->gas = $this ->gas + $add_gas;
		$this->speed = $this ->speed + $this ->gas * 10; 
		return $this ->speed; 
	}
}