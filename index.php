<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки
require_once 'Interfaces.php'; 
require_once 'Product.php'; 
require_once 'Class.php'; 

$Ford = new AutoCar('Ford');   // новая машина марки Ford
$Toyota = new AutoCar('Toyota');  // новая машина марки Toyota
$BMW = new AutoCar('BMW');    // новая машина марки BMW
$BMW->color = 'red';        // BMW покрасили красным цветом 
$BMW->type = 'passenger';  //  BMW сделали легковым авто
$BMW->category = 'авто'; 
echo $BMW ->category . '<br />';      // узнали категорию
echo $BMW->type . '<br />';    // узнали, что BMW - легковая
echo 'поворот налево на: ' . $BMW ->courseNew(45, 5) . '<br />';    // повернули руль на 45 град. и держим 5 сек. - получили новый курс
echo $BMW ->speed(5, 3) . ' км/ч <br />';     // надавили на газ усилием в 5 ед. и получили скорость
echo AutoCar::$staticCounter . '<br /><br />';    // определили кол-во новых авто

$tv_LG = new Televisor();       // новый телевизор LG
$tv_samsung = new Televisor();    // новый телевизор Samsung
$tv_samsung->brand = 'samsung';    // марка - samsung
$tv_samsung->type = 'LSD TV';    // тип - жидкокристаллический
$tv_samsung->setDiagonal('127cm');    // диагональ - 127 сантиметров
echo $tv_samsung->brand . '<br />';      // узнали марку
echo $tv_samsung->type . '<br />';    // узнали тип
echo $tv_samsung->getDiagonal() . '<br />';  // узнали диагональ 
$tv_samsung->turnOn();   // нажали на кнопку и включили
echo '<br /><br />';  // узнали диагональ 

$parker = new Ballpen();    // новая ручка
$rayter = new Ballpen();    // новая ручка
$rayter->brand = 'rayter';    // марка новой ручки - rayter
$rayter->type = 'gel';    // тип новой ручки - гелевая
$rayter->turnOn();      // нажал на кнопку на обратном концце ручки, видно стержень
$rayter->turnOff();      // нажал на кнопку, стержень не видно
$rayter ->setPrice(100);            // установили цену
$rayter ->discount = 5;            // установили скидку
echo $rayter->brand . '<br />';   //  узнал марку ручки
echo $rayter ->getPrice() . ' руб. <br />';   // определили цену
echo $rayter->type . '<br />';    // узнал тип - гелевая
var_dump($rayter ->getRod());    // вынул стержень
echo '<br />'; 
var_dump($rayter ->insertRod());    // вставил стержень
echo '<br />'; 
echo $rayter ->write('Hello world!') . '<br /><br />';    // написал слова

$Krya = new Duck('Krya');      // новая утка, зовут Кря
Duck::$living = true;
$Donald = new Duck('Donald');    // новая утка, зовут Дональд
$Donald ->floats();          // Дональд плавает
$Donald ->walking();       // Дональд ходит
$Donald ->fly();          // Дональд летает
echo 'утка в воде? '; 
var_dump($Donald ->onTheAir); 
echo '<br />'; 
echo $Donald ->name;   // узнаем имя Дональда